!     LCMEXC(ISYM,IEXC) - whether transition moments involving IEXC:th
!     state in ISYM symmetry should be computed.
      INTEGER NCOREORBS, ICOREORBS, MAXCORES, MXEXQR1
      PARAMETER (MAXCORES=8)
      PARAMETER (MXEXQR1=30 )
      DOUBLE PRECISION EXCITACORE
      LOGICAL LDOCORE,LDOVALE
      COMMON /INFXAS/NCOREORBS(8),ICOREORBS(MAXCORES,8),
     &               EXCITACORE(8,MXEXQR1,2),
     &               LDOCORE,LDOVALE,
     &               NPPCNVCORE(8), NPPSIMCORE(8),NPPSTVCORE(8)

